//
//  AliasViewController.h
//  iNextcoin
//
//  Created by Riccardo Rossi on 14/01/14.
//  Copyright (c) 2014 Riccardo Rossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AliasViewController : UIViewController{
    
    IBOutlet UIButton *getalias;
    IBOutlet UIButton *getid;
    IBOutlet UITextField *alias;
    IBOutlet UITextView *viewalias;
    IBOutlet UITextField *account;
    IBOutlet UILabel *accountid;
    IBOutlet UIImageView *server;
}
-(IBAction) checkalias;
-(IBAction) checkid;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;


@end
