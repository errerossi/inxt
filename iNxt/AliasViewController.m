//
//  AliasViewController.m
//  iNextcoin
//
//  Created by Riccardo Rossi on 14/01/14.
//  Copyright (c) 2014 Riccardo Rossi. All rights reserved.
//

#import "AliasViewController.h"
#import "SWRevealViewController.h"
#import "Preferences.h"

@interface AliasViewController ()
@property (strong, nonatomic) Preferences *preferences;
@end

@implementation AliasViewController

-(IBAction) checkid {
    viewalias.text = @"";
    [account resignFirstResponder];
    NSString *apiUrl = [NSString stringWithFormat:@"%@/nxt?requestType=getState", self.preferences.server];
    NSURL *url = [NSURL URLWithString:apiUrl];
    NSData *connesso = [NSData dataWithContentsOfURL:url];
    if (connesso!= nil) {
        
        NSString *APIServer = [NSString stringWithFormat:@"%@/nxt?requestType=getAliases&account=%@",self.preferences.server,account.text];
       NSURL *url = [NSURL URLWithString:APIServer];
       NSData *data = [NSData dataWithContentsOfURL:url];
 
    if (data){
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSArray *aliases = [json objectForKey:@"aliases"];
        for(id singlealias in aliases){

        NSString *aliaslist = [NSString stringWithFormat:@"%@",[singlealias objectForKey:@"aliasName"]];
            NSString *urilist = [NSString stringWithFormat:@"%@",[singlealias objectForKey:@"aliasURI"]];
            NSString *allAlias;
            if ([urilist isEqualToString:@""]) {
                allAlias = [NSString stringWithFormat:@"%@",aliaslist];
            }else{
            allAlias = [NSString stringWithFormat:@"%@ | %@",aliaslist,urilist];
            }
            viewalias.text = [NSString stringWithFormat:@"-%@\n%@",allAlias,viewalias.text];
    }
    }}
}

-(IBAction) checkalias {
    [alias resignFirstResponder];
    NSString *apiUrl = [NSString stringWithFormat:@"%@/nxt?requestType=getState", self.preferences.server];
    NSURL *url = [NSURL URLWithString:apiUrl];
    NSData *connesso = [NSData dataWithContentsOfURL:url];
    if (connesso!= nil) {
        NSString *APIServer = [NSString stringWithFormat:@"%@/nxt?requestType=getAlias&aliasName=%@",self.preferences.server,alias.text];
        
        NSURL *url = [NSURL URLWithString:APIServer];
        NSData *data = [NSData dataWithContentsOfURL:url];

    if (data){
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSString *accountID = [NSString stringWithFormat:@"%@",[json objectForKey:@"account"]];
        accountid.text = accountID;
    }
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
        self.preferences = [Preferences sharedPreferences];
    self.title = @"Alias";
    
    
    if (self.preferences.server == NULL) {
        self.preferences.server = @"http://xyzzyx.vps.nxtcrypto.org:7876";
    }

    
    NSString *apiUrl = [NSString stringWithFormat:@"%@/nxt?requestType=getState", self.preferences.server];
    NSURL *url = [NSURL URLWithString:apiUrl];
    NSData *connesso = [NSData dataWithContentsOfURL:url];
    if (connesso!= nil) {
        server.image = [UIImage imageNamed: @"server_online.png"];
    }else{
        server.image = [UIImage imageNamed: @"server_offline.png"];
    }

    
    // Change button color
    //  _sidebarButton.tintColor = [UIColor colorWithWhite:0.96f alpha:0.2f];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
