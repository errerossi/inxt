//
//  AppDelegate.h
//  iNxt
//
//  Created by Riccardo Rossi on 19/01/14.
//  Copyright (c) 2014 Riccardo Rossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
