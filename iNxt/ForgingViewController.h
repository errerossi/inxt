//
//  ForgingViewController.h
//  iNxt
//
//  Created by Riccardo Rossi on 04/02/14.
//  Copyright (c) 2014 Riccardo Rossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgingViewController : UIViewController{
    
    IBOutlet UIButton *forge;
    IBOutlet UIButton *getforging;
    IBOutlet UIButton *stopforge;
    IBOutlet UITextField *passhprase;
    IBOutlet UIImageView *server;
    IBOutlet UITextView *forgingresponse;
}
-(IBAction) forge;
-(IBAction) stopforge;
-(IBAction) getforging;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;



@end
