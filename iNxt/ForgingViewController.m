//
//  ForgingViewController.m
//  iNxt
//
//  Created by Riccardo Rossi on 04/02/14.
//  Copyright (c) 2014 Riccardo Rossi. All rights reserved.
//

#import "ForgingViewController.h"
#import "SWRevealViewController.h"
#import "Preferences.h"

@interface ForgingViewController ()
@property (strong, nonatomic) Preferences *preferences;
@end

@implementation ForgingViewController



/*
 Added startForging and stopForging API requests.
 Parameters: secretPhrase, required for both starting and stopping.
 */


-(IBAction) forge {
    [passhprase resignFirstResponder];
    NSString *apiUrl = [NSString stringWithFormat:@"%@/nxt?requestType=getState", self.preferences.server];
    NSURL *url = [NSURL URLWithString:apiUrl];
    NSData *connesso = [NSData dataWithContentsOfURL:url];
    if (connesso!= nil) {
        NSString *Post = [[NSString alloc] initWithFormat:@"requestType=startForging&secretPhrase=%@", passhprase.text];
        NSString *APIServer = [NSString stringWithFormat:@"%@/nxt",self.preferences.server];
        NSURL *Url = [NSURL URLWithString:APIServer];
        NSData *PostData = [Post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[PostData length]];
        NSMutableURLRequest *Request = [[NSMutableURLRequest alloc] init];
        [Request setURL:Url];
        [Request setHTTPMethod:@"POST"];
        [Request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [Request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [Request setHTTPBody:PostData];
        NSData *returnData = [NSURLConnection sendSynchronousRequest:Request returningResponse:nil error:nil];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:nil];
        NSString *forgedeadline = [json objectForKey:@"deadline"];
        forgingresponse.text = [NSString stringWithFormat:@"Deadline: %@",forgedeadline];
    }else{
        forgingresponse.text = @"Server Offline, you can't forge";
    }
    
}
-(IBAction) stopforge {
    [passhprase resignFirstResponder];
    NSString *apiUrl = [NSString stringWithFormat:@"%@/nxt?requestType=getState", self.preferences.server];
    NSURL *url = [NSURL URLWithString:apiUrl];
    NSData *connesso = [NSData dataWithContentsOfURL:url];
    if (connesso!= nil) {
        NSString *Post = [[NSString alloc] initWithFormat:@"requestType=stopForging&secretPhrase=%@", passhprase.text];
        NSString *APIServer = [NSString stringWithFormat:@"%@/nxt",self.preferences.server];
        NSURL *Url = [NSURL URLWithString:APIServer];
        NSData *PostData = [Post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[PostData length]];
        NSMutableURLRequest *Request = [[NSMutableURLRequest alloc] init];
        [Request setURL:Url];
        [Request setHTTPMethod:@"POST"];
        [Request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [Request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [Request setHTTPBody:PostData];
        NSData *returnData = [NSURLConnection sendSynchronousRequest:Request returningResponse:nil error:nil];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:nil];
        if ([[json objectForKey:@"foundAndStopped"] boolValue]) {
            forgingresponse.text = @"You are no more forging";
        }else{
            forgingresponse.text = @"You aren't forging";
        }
    }else{
        forgingresponse.text = @"Server Offline, you are not forging";
    }
    
}

-(IBAction) getforging {
    [passhprase resignFirstResponder];
    NSString *apiUrl = [NSString stringWithFormat:@"%@/nxt?requestType=getState", self.preferences.server];
    NSURL *url = [NSURL URLWithString:apiUrl];
    NSData *connesso = [NSData dataWithContentsOfURL:url];
    if (connesso!= nil) {
        NSString *Post = [[NSString alloc] initWithFormat:@"requestType=getForging&secretPhrase=%@", passhprase.text];
        NSString *APIServer = [NSString stringWithFormat:@"%@/nxt",self.preferences.server];
        NSURL *Url = [NSURL URLWithString:APIServer];
        NSData *PostData = [Post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[PostData length]];
        NSMutableURLRequest *Request = [[NSMutableURLRequest alloc] init];
        [Request setURL:Url];
        [Request setHTTPMethod:@"POST"];
        [Request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [Request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [Request setHTTPBody:PostData];
        NSData *returnData = [NSURLConnection sendSynchronousRequest:Request returningResponse:nil error:nil];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:nil];
        NSString *forgedeadline = [json objectForKey:@"deadline"];
        if (!forgedeadline) {
         forgingresponse.text = @"You aren't forging";
        }else{
            forgingresponse.text = [NSString stringWithFormat:@"Deadline: %@",forgedeadline];
        }
    }else{
        forgingresponse.text = @"Server Offline, you are not forging";
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.preferences = [Preferences sharedPreferences];
    
    self.title = @"Forge";
    
    if (self.preferences.server == NULL) {
        self.preferences.server = @"http://xyzzyx.vps.nxtcrypto.org:7876";
    }



    
    NSString *apiUrl = [NSString stringWithFormat:@"%@/nxt?requestType=getState", self.preferences.server];
    NSURL *url = [NSURL URLWithString:apiUrl];
    NSData *connesso = [NSData dataWithContentsOfURL:url];
    if (connesso!= nil) {
        server.image = [UIImage imageNamed: @"server_online.png"];
    }else{
        server.image = [UIImage imageNamed: @"server_offline.png"];
    }

    
    
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

