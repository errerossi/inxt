//
//  ExchangesViewController.h
//  iNxt
//
//  Created by Riccardo Rossi on 28/01/14.
//  Copyright (c) 2014 Riccardo Rossi. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MainViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
        IBOutlet UIButton *refresh;
 }

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property(weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *exchanges;
-(IBAction) refresh;


@end
