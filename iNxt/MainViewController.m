//
//  ExchangesViewController.m
//  iNxt
//
//  Created by Riccardo Rossi on 28/01/14.
//  Copyright (c) 2014 Riccardo Rossi. All rights reserved.
//

#import "MainViewController.h"
#import "SWRevealViewController.h"

@interface MainViewController ()

@property (strong, nonatomic) dispatch_queue_t exchangeQueue;

@end

@implementation MainViewController

-(dispatch_queue_t)exchangeQueue {
  @synchronized(self) {
    if (!_exchangeQueue) {
      _exchangeQueue = dispatch_queue_create("refresh", DISPATCH_QUEUE_CONCURRENT);
    }
  }
	return _exchangeQueue;
}

-(void)bter {
  static NSString *const BaseURLString = @"http://data.bter.com/api/1/ticker/nxt_btc";
  dispatch_async(self.exchangeQueue, ^{
    NSString *apiUrl = [NSString stringWithFormat:@"%@", BaseURLString];
    NSURL *url = [NSURL URLWithString:apiUrl];
    NSData *dataj = [NSData dataWithContentsOfURL:url];
      NSString *prezzo;
    if (dataj!= nil) {
          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dataj options:kNilOptions error:nil];
          NSString *last = [json objectForKey:@"last"];
          prezzo = [NSString stringWithFormat:@"BTER            %f BTC",[last floatValue]];
    }else{
        prezzo = @"ERROR";
    }
    dispatch_async(dispatch_get_main_queue(), ^{
      [self.exchanges replaceObjectAtIndex:0 withObject:prezzo];
      [self.tableView reloadData];
    });
  });
}

-(void)bter2 {
    static NSString *const BaseURLString = @"http://data.bter.com/api/1/ticker/nxt_cny";
    dispatch_async(self.exchangeQueue, ^{
        NSString *apiUrl = [NSString stringWithFormat:@"%@", BaseURLString];
        NSURL *url = [NSURL URLWithString:apiUrl];
        NSData *dataj = [NSData dataWithContentsOfURL:url];
        NSString *prezzo;
        if (dataj!= nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dataj options:kNilOptions error:nil];
            NSString *last = [json objectForKey:@"last"];
            prezzo = [NSString stringWithFormat:@"BTER            %f CNY",[last floatValue]];
        }else{
            prezzo = @"ERROR";
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.exchanges replaceObjectAtIndex:4 withObject:prezzo];
            [self.tableView reloadData];
        });
    });
}


-(void)dgex {
  static NSString *const BaseURLString = @"http://dgex.com/API/trades3h.json";
  dispatch_async(self.exchangeQueue, ^{
    NSString *apiUrl = [NSString stringWithFormat:@"%@", BaseURLString];
    NSURL *url = [NSURL URLWithString:apiUrl];
    NSData* dataj = [NSData dataWithContentsOfURL:url];
      NSString *prezzo;
      if (dataj!= nil) {
          NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataj options:kNilOptions error:nil];
          NSArray *ticker = [json objectForKey:@"ticker"];
          if (ticker != nil || [ticker count] != 0) {
          prezzo = [NSString stringWithFormat:@"DGEX            %f BTC",[[ticker[0] objectForKey:@"unitprice"] floatValue]];
          }else{
           prezzo = @"ERROR";
          }
      }else{
          prezzo = @"ERROR";
      }
    dispatch_async(dispatch_get_main_queue(), ^{
      [self.exchanges replaceObjectAtIndex:7 withObject:prezzo];
      [self.tableView reloadData];
    });
  });
}

-(void)cryptsy {
    static NSString *const BaseURLString = @"http://pubapi.cryptsy.com/api.php?method=singlemarketdata&marketid=159";
    dispatch_async(self.exchangeQueue, ^{
        NSString *apiUrl = [NSString stringWithFormat:@"%@", BaseURLString];
        NSURL *url = [NSURL URLWithString:apiUrl];
        NSData* dataj = [NSData dataWithContentsOfURL:url];
        NSString *prezzo;
        if (dataj!= nil) {
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataj options:kNilOptions error:nil];
            NSString *last = [json valueForKeyPath:@"return.markets.NXT.lasttradeprice"];
            prezzo = [NSString stringWithFormat:@"Cryptsy         %f BTC",[last floatValue]];
        }else{
            prezzo = @"ERROR";
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.exchanges replaceObjectAtIndex:1 withObject:prezzo];
            [self.tableView reloadData];
        });
    });
}



-(void)poloniex {
    static NSString *const BaseURLString = @"https://poloniex.com/public?command=returnTicker";
    dispatch_async(self.exchangeQueue, ^{
        NSString *apiUrl = [NSString stringWithFormat:@"%@", BaseURLString];
        NSURL *url = [NSURL URLWithString:apiUrl];
        NSData* dataj = [NSData dataWithContentsOfURL:url];
        NSString *prezzo;
        if (dataj!= nil) {
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataj options:kNilOptions error:nil];
            NSDictionary *nxt = [json objectForKey:@"BTC_NXT"];
            prezzo = [NSString stringWithFormat:@"Poloniex        %f BTC",[[nxt objectForKey:@"last"] floatValue]];
        }else{
            prezzo = @"ERROR";
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.exchanges replaceObjectAtIndex:2 withObject:prezzo];
            [self.tableView reloadData];
        });
    });
}


-(void)hitbtc {
    static NSString *const BaseURLString = @"http://api.hitbtc.com/api/1/public/NXTBTC/ticker";
    dispatch_async(self.exchangeQueue, ^{
        NSString *apiUrl = [NSString stringWithFormat:@"%@", BaseURLString];
        NSURL *url = [NSURL URLWithString:apiUrl];
        NSData* dataj = [NSData dataWithContentsOfURL:url];
        NSString *prezzo;
        if (dataj!= nil) {
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataj options:kNilOptions error:nil];
            NSString *last = [json objectForKey:@"last"];
            prezzo = [NSString stringWithFormat:@"HitBTC        %f BTC",[last floatValue]];
        }else{
            prezzo = @"ERROR";
        }
       
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.exchanges replaceObjectAtIndex:3 withObject:prezzo];
            [self.tableView reloadData];
        });
    });
}

-(void)bittrex {
    static NSString *const BaseURLString = @"https://bittrex.com/api/v1.1/public/getticker?market=BTC-NXT";
    dispatch_async(self.exchangeQueue, ^{
        NSString *apiUrl = [NSString stringWithFormat:@"%@", BaseURLString];
        NSURL *url = [NSURL URLWithString:apiUrl];
        NSData* dataj = [NSData dataWithContentsOfURL:url];
        NSString *prezzo;
        if (dataj!= nil) {
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataj options:kNilOptions error:nil];
            NSDictionary *nxt = [json objectForKey:@"result"];
            prezzo = [NSString stringWithFormat:@"Bittrex        %f BTC",[[nxt objectForKey:@"Last"] floatValue]];
        }else{
            prezzo = @"ERROR";
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.exchanges replaceObjectAtIndex:5 withObject:prezzo];
            [self.tableView reloadData];
        });
    });
}

-(void)cryptsy2 {
    static NSString *const BaseURLString = @"http://pubapi.cryptsy.com/api.php?method=singlemarketdata&marketid=162";
    dispatch_async(self.exchangeQueue, ^{
        NSString *apiUrl = [NSString stringWithFormat:@"%@", BaseURLString];
        NSURL *url = [NSURL URLWithString:apiUrl];
        NSData *dataj = [NSData dataWithContentsOfURL:url];
        NSString *prezzo;
        if (dataj!= nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dataj options:kNilOptions error:nil];
            NSString *last = [json valueForKeyPath:@"return.markets.NXT.lasttradeprice"];
            prezzo = [NSString stringWithFormat:@"Cryptsy         %f LTC",[last floatValue]];
        }else{
            prezzo = @"ERROR";
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.exchanges replaceObjectAtIndex:6 withObject:prezzo];
            [self.tableView reloadData];
        });
    });

}



//lazy instantiation
-(NSMutableArray*)exchanges
{
    if (_exchanges == nil) {
        _exchanges = [[NSMutableArray alloc]init];
    }
    return _exchanges;
}

//table view
-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.exchanges count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"identifier"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"identifier"];
    }
    cell.textLabel.text =  self.exchanges [indexPath.row];
    
    
    return cell;
}

-(void)fetch {
    [self.exchanges insertObject:@"" atIndex:0];
    [self.exchanges insertObject:@"" atIndex:1];
    [self.exchanges insertObject:@"" atIndex:2];
    [self.exchanges insertObject:@"" atIndex:3];
    [self.exchanges insertObject:@"" atIndex:4];
    [self.exchanges insertObject:@"" atIndex:5];
    [self.exchanges insertObject:@"" atIndex:6];
    [self.exchanges insertObject:@"" atIndex:7];
   [self bter];
    [self cryptsy];
    [self poloniex];
    [self hitbtc];
   [self bter2];
    [self bittrex];
    [self cryptsy2];
  //  [self dgex];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    self.title = @"Exchanges";
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self fetch];
    
     
}

-(IBAction) refresh {
    [self.exchanges removeAllObjects];
    [self.tableView reloadData];
    [self fetch];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
