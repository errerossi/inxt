//
//  Preferences.h
//  NexText
//
//  Created by Wolfgang on 1/21/14.
//  Copyright (c) 2014 Riccardo Rossi. All rights reserved.
//
@import Foundation;

@interface Preferences : NSObject

@property (strong, nonatomic) NSString *account;
@property (strong, nonatomic) NSString *server;
@property (strong, nonatomic) NSString *currency;

+(instancetype)sharedPreferences;

@end
