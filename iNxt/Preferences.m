//
//  Preferences.m
//  NexText
//
//  Created by Wolfgang on 1/21/14.
//  Copyright (c) 2014 Riccardo Rossi. All rights reserved.
//

#import "Preferences.h"

NSString *const alreadySavedDefaultKey = @"PreferencesAlreadySaved";
NSString *const accountDefaultKey = @"PreferencesAccount";
NSString *const serverDefaultKey = @"PreferencesServer";
NSString *const currencyDefaultKey = @"PreferencesCurrency";

@interface Preferences ()

@property (strong, nonatomic) NSUserDefaults *defaults;

@end

@implementation Preferences

-(instancetype)init
{
    self = [super init];
    if (self) {
		if ([self.defaults boolForKey:alreadySavedDefaultKey]) {
			self.account = [self.defaults stringForKey:accountDefaultKey];
            self.server = [self.defaults stringForKey:serverDefaultKey];
            self.currency = [self.defaults stringForKey:currencyDefaultKey];
		}
    }
    return self;
}

-(NSUserDefaults *)defaults {
	if (!_defaults) {
		_defaults = [NSUserDefaults standardUserDefaults];
	}
	return _defaults;
}

-(void)saveDefaults {
	[self.defaults setBool:YES forKey:alreadySavedDefaultKey];
    [self.defaults synchronize];
}

-(void)setAccount:(NSString *)account {
	_account = account;
	[self.defaults setObject:account forKey:accountDefaultKey];
	[self saveDefaults];
}

-(void)setServer:(NSString *)server {
	_server = server;
	[self.defaults setObject:server forKey:serverDefaultKey];
	[self saveDefaults];
}

-(void)setCurrency:(NSString *)currency {
    _currency = currency;
    [self.defaults setObject:currency forKey:currencyDefaultKey];
    [self saveDefaults];
}

+(instancetype)sharedPreferences {
	static Preferences *sharedPreferences;
	if (!sharedPreferences) {
		sharedPreferences = [Preferences new];
	}
	return sharedPreferences;
}

@end
