//
//  SettingsViewController.h
//  iNxt
//
//  Created by Riccardo Rossi on 14/05/14.
//  Copyright (c) 2014 Riccardo Rossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController{
    IBOutlet UIImageView *server;
    IBOutlet UIButton *save;
    IBOutlet UITextField *serveraddress;
    IBOutlet UITextField *account;
    IBOutlet UISegmentedControl *Segment;
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
-(IBAction) save;


@end
