//
//  SettingsViewController.m
//  iNxt
//
//  Created by Riccardo Rossi on 28/01/14.
//  Copyright (c) 2014 Riccardo Rossi. All rights reserved.
//

#import "SettingsViewController.h"
#import "SWRevealViewController.h"
#import "Preferences.h"

@interface SettingsViewController ()

@property (strong, nonatomic) Preferences *preferences;

@end

@implementation SettingsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.preferences = [Preferences sharedPreferences];
    self.title = @"Settings";
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    if(self.preferences.currency==NULL){
        self.preferences.currency=@"USD";
    }else if ([self.preferences.currency isEqual:@"EUR"]){
        Segment.selectedSegmentIndex = 1;
    }

    
    if (self.preferences.server == NULL) {
    self.preferences.server = @"http://xyzzyx.vps.nxtcrypto.org:7876";
       serveraddress.text = self.preferences.server;
    }else{
        serveraddress.text = self.preferences.server;
    }
    
    if(self.preferences.account!=NULL){
        account.text = self.preferences.account;
    }
    
    NSString *apiUrl = [NSString stringWithFormat:@"%@/nxt?requestType=getState", serveraddress.text];
    NSURL *url = [NSURL URLWithString:apiUrl];
    NSData *connesso = [NSData dataWithContentsOfURL:url];
    if (connesso!= nil) {
        server.image = [UIImage imageNamed: @"server_online.png"];
    }else{
        server.image = [UIImage imageNamed: @"server_offline.png"];
    }
    
    
    
}

-(IBAction)save{
    [serveraddress resignFirstResponder];
    [account resignFirstResponder];
    if (serveraddress.text != self.preferences.server) {
        NSString *apiUrl = [NSString stringWithFormat:@"%@/nxt?requestType=getState", serveraddress.text];
        NSURL *url = [NSURL URLWithString:apiUrl];
        NSData *connesso = [NSData dataWithContentsOfURL:url];
        
        
        if (connesso!= nil) {
            self.preferences.server = serveraddress.text;
            server.image = [UIImage imageNamed: @"server_online.png"];
        }else{
            server.image = [UIImage imageNamed: @"server_offline.png"];
        }
        
        
        }
        if (account.text != self.preferences.account) {
            self.preferences.account = account.text;
        }
    
   if(![[Segment titleForSegmentAtIndex:[Segment selectedSegmentIndex]] isEqual:self.preferences.currency]){
        self.preferences.currency=[Segment titleForSegmentAtIndex:[Segment selectedSegmentIndex]];
    }

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end