//
//  ViewController.h
//  SidebarDemo
//
//  Created by Simon on 28/6/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletonViewController : UIViewController{
    
    IBOutlet UIButton *check;
    IBOutlet UILabel *balance;
    IBOutlet UILabel *usd;
    IBOutlet UITextView *transaction;
    IBOutlet UIImageView *server;

}
-(IBAction) check;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;



@end
