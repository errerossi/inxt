//
//  ViewController.m
//  SidebarDemo
//
//  Created by Simon on 28/6/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "WalletonViewController.h"
#import "SWRevealViewController.h"
#import "Preferences.h"

@interface WalletonViewController ()
@property (strong, nonatomic) Preferences *preferences;
@end

@implementation WalletonViewController



-(IBAction) check {
    
    transaction.text = @"";
    NSString *apiUrl = [NSString stringWithFormat:@"%@/nxt?requestType=getState", self.preferences.server];
    NSURL *url = [NSURL URLWithString:apiUrl];
    NSData *connesso = [NSData dataWithContentsOfURL:url];
    if (connesso!= nil) {
        if(self.preferences.account!=NULL){
            
            NSString *Post = [[NSString alloc] initWithFormat:@"requestType=getBalance&account=%@",self.preferences.account];
            NSString *APIServer = [NSString stringWithFormat:@"%@/nxt",self.preferences.server];
            NSURL *Url = [NSURL URLWithString:APIServer];
            NSData *PostData = [Post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[PostData length]];
            NSMutableURLRequest *Request = [[NSMutableURLRequest alloc] init];
            [Request setURL:Url];
            [Request setHTTPMethod:@"POST"];
            [Request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [Request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [Request setHTTPBody:PostData];
            NSData *data = [NSURLConnection sendSynchronousRequest:Request returningResponse:nil error:nil];
            
            if (data){
                NSDictionary* json2 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                NSString *balances = [NSString stringWithFormat:@"%@",[json2 objectForKey:@"balanceNQT"]];
                balance.text = [NSString stringWithFormat:@"%ld NXT",[balances integerValue]/100000000];
                
                
                NSString* BaseURLString = [NSString stringWithFormat:@"https://api.bitcoinaverage.com/ticker/global/%@/", self.preferences.currency];
                NSString *apiUrl = [NSString stringWithFormat:@"%@", BaseURLString];
                NSURL *url = [NSURL URLWithString:apiUrl];
                NSData* dataj = [NSData dataWithContentsOfURL:url];
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataj options:kNilOptions error:nil];
                NSString *prezzousd = [json objectForKey:@"24h_avg"];
                static NSString *const BaseURLString2 = @"http://data.bter.com/api/1/ticker/nxt_btc";
                NSString *apiUrl2 = [NSString stringWithFormat:@"%@", BaseURLString2];
                NSURL *url2 = [NSURL URLWithString:apiUrl2];
                NSData* dataj2 = [NSData dataWithContentsOfURL:url2];
                NSDictionary* json20 = [NSJSONSerialization JSONObjectWithData:dataj2 options:kNilOptions error:nil];
                NSString *prezzobtc = [json20 objectForKey:@"avg"];
                usd.text = [NSString stringWithFormat:@"%f %@",[balance.text integerValue] * [prezzobtc floatValue] * [prezzousd floatValue],self.preferences.currency];
                
                
                NSString *Post = [[NSString alloc] initWithFormat:@"requestType=getAccountTransactionIds&account=%@&timestamp=0", self.preferences.account];
                NSString *APIServer = [NSString stringWithFormat:@"%@/nxt",self.preferences.server];
                NSURL *Url = [NSURL URLWithString:APIServer];
                NSData *PostData = [Post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[PostData length]];
                NSMutableURLRequest *Request = [[NSMutableURLRequest alloc] init];
                [Request setURL:Url];
                [Request setHTTPMethod:@"POST"];
                [Request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                [Request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                [Request setHTTPBody:PostData];
                NSData *data = [NSURLConnection sendSynchronousRequest:Request returningResponse:nil error:nil];
                
                if (data){
                    NSDictionary* jsonTrans = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                    NSArray *arrayTrans = [jsonTrans objectForKey:@"transactionIds"];
                    for(id Transaction in arrayTrans){
                        
                        NSString *Post = [[NSString alloc] initWithFormat:@"requestType=getTransaction&transaction=%@",Transaction];
                        NSString *APIServer = [NSString stringWithFormat:@"%@/nxt",self.preferences.server];
                        NSURL *Url = [NSURL URLWithString:APIServer];
                        NSData *PostData = [Post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[PostData length]];
                        NSMutableURLRequest *Request = [[NSMutableURLRequest alloc] init];
                        [Request setURL:Url];
                        [Request setHTTPMethod:@"POST"];
                        [Request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                        [Request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                        [Request setHTTPBody:PostData];
                        NSData *data = [NSURLConnection sendSynchronousRequest:Request returningResponse:nil error:nil];
                        
                        if (data){
                            NSDictionary* jsonTransaction = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                            //NSString *timestamp = [NSString stringWithFormat:@"%@",[jsonTransaction objectForKey:@"timestamp"]];
                            NSString *sender = [NSString stringWithFormat:@"%@",[jsonTransaction objectForKey:@"sender"]];
                            NSString *recipient = [NSString stringWithFormat:@"%@",[jsonTransaction objectForKey:@"recipient"]];
                            NSString *senderRS = [NSString stringWithFormat:@"%@",[jsonTransaction objectForKey:@"senderRS"]];
                            NSString *recipientRS = [NSString stringWithFormat:@"%@",[jsonTransaction objectForKey:@"recipientRS"]];
                            NSString *amount = [NSString stringWithFormat:@"%@",[jsonTransaction objectForKey:@"amountNQT"]];
                            if([sender isEqual:self.preferences.account]||[senderRS isEqual:self.preferences.account]){
                                if([recipient isEqual:@"(null)"]){recipient=@"GENESIS";}
                                transaction.text = [NSString stringWithFormat:@"-> %@ - %ld\n%@",recipient,[amount integerValue]/100000000,transaction.text];
                            }
                            if([recipient isEqual:self.preferences.account]||[recipientRS isEqual:self.preferences.account]){
                                transaction.text = [NSString stringWithFormat:@"<- %@ - %ld\n%@",sender,[amount integerValue]/100000000,transaction.text];
                            }
                            
                           
                        }
                        
                    }
                    
                    
                }else{
                    balance.text = @"";
                }
            }
            
        }else{
            transaction.text = @"Please add account in settings";
        }
    }

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.preferences = [Preferences sharedPreferences];
    
    self.title = @"Online Wallet";
    
    if (self.preferences.server == NULL) {
    self.preferences.server = @"http://xyzzyx.vps.nxtcrypto.org:7876";
    }

    NSString *apiUrl = [NSString stringWithFormat:@"%@/nxt?requestType=getState", self.preferences.server];
    NSURL *url = [NSURL URLWithString:apiUrl];
    NSData *connesso = [NSData dataWithContentsOfURL:url];
    if (connesso!= nil) {
        server.image = [UIImage imageNamed: @"server_online.png"];
    }else{
        server.image = [UIImage imageNamed: @"server_offline.png"];
    }

    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
  
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
