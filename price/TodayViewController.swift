//
//  TodayViewController.swift
//  price
//
//  Created by Riccardo Rossi on 03/12/14.
//  Copyright (c) 2014 Riccardo Rossi. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
    @IBOutlet var price: UILabel!
    var old = String()
    var check = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        self.updateCheck()
        old=check
        self.price.text = "NXT \(check) BTC"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)!) {
        // Perform any setup necessary in order to update the view.
        self.updateCheck()
        var result: NSComparisonResult = old.compare(check)
        if(result == NSComparisonResult.OrderedSame) {
            completionHandler(NCUpdateResult.NoData)
        }
        else{
            old=check
            self.price.text = "NXT \(check) BTC"
            completionHandler(NCUpdateResult.NewData)
        }
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
    }
    
    func updateCheck() {
    let urlPath: String = "http://data.bter.com/api/1/ticker/nxt_btc"
    var url: NSURL = NSURL(string: urlPath)!
    var request1: NSURLRequest = NSURLRequest(URL: url)
    var response: AutoreleasingUnsafeMutablePointer<NSURLResponse?
    >=nil
    var error: NSErrorPointer = nil
    var dataVal: NSData =  NSURLConnection.sendSynchronousRequest(request1, returningResponse: response, error:nil)!
    var err: NSError
    var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(dataVal, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
    var last : String = jsonResult["last"] as String
    check=last
    }
    
    
}
